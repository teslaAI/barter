<div class="search-form">
    <form action="{{ url('/') }}" method="get">
        <div class="search-form-check-wrapper">
            <div class="search-form-opt">
                <input type="checkbox" class="checkbox" name="option" value="1" @click="assign_option()" id="opt" />
                <label for="opt">
                    <span class="search-form-opt-wont">Хотят</span>
                    <span class="search-form-opt-have">Предлагают</span>
                </label>
            </div>
        </div>
        <input name="search" value="{{ Request::get('search') }}" autocomplete="off" type="text" placeholder="" v-model="keywords" class="search-form-input" list="data">
        <div class="location-link-resp"></div>
        <span>
            <button class="search-form-submit">Найти</button>
        </span>
        <div v-show="results.length">
            <ul class="search-form-results" id="data">
                <li v-for="result in results" :key="result.id" v-html="highlight(searchmethod ? result.title : result.name)"></li>
            </ul>
        </div>
    </form>
</div>