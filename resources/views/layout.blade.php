<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    @yield('meta')
    <title>@yield('title', "Kerda &mdash; первый сайт объявлений и бартерных сделок в Ингушетии")</title>
    <link rel="stylesheet" href="{{ version('css/bundle.css') }}">
    <link rel="stylesheet" href="{{ version('css/main.css') }}">
    <link rel="icon" href="{{ asset('img/favicon.ico') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body class="{{ Request::is('chats/*') ? 'chat-page' : (Request::is('show/*') ? 'single-page' : '') }}">
<header class="clearfix {{ Request::is('/') || Request::is('search') || Request::is('tag/*') || Request::is('category/*') ? 'home-page' : '' }}">
    <div class="wrapper">
        <div class="header-right">
            @if (Auth::check())
                <a class="publish orangebutton" href="{{ action('AdController@create') }}">Добавить сделку</a>
                <div class="header-profile">
                    <a href="" class="header-profile-img {{ $messages ? 'notification' : '' }}">
                        <img src="{{ Auth::user()->userAvatarURL }}" alt="">
                    </a>
                    <div class="popup profile-popup">
                        <div class="popup-close"></div>
                        <div class="popup-tail">
                            <div class="resp-title-wrapper">
                                <div class="resp-title">Мой профиль</div>
                            </div>
                            <a href="{{ url('author', Auth::user()->id) }}" class="popup-name blacklink">{{ Auth::user()->name }}</a>
                            <span class="popup-email">{{ Auth::user()->email }}</span>
                            <ul class="popup-menu">
                                <li><a class="blacklink header-chats-resp {{ $messages ? 'notification' : '' }}" href="{{ url('chats/all') }}" >Мои чаты</a></li>
                                <li><a class="blacklink" href="{{ url('/user/status=active') }}">Мои сделки</a></li>
                                <li><a class="blacklink" href="{{ url('/user/status=favorite') }}">Избранное</a></li>
                                <li><a class="blacklink" href="{{ url('/settings') }}">Настройки профиля</a></li>
                            </ul>
                            <a class="exit blacklink" href="{{ url('/logout') }}">Выйти</a>
                        </div>
                    </div>
                </div>
                <a href="{{ url('chats/all') }}" class="header-chats {{ $messages ? 'notification' : '' }} blacklink">Мои чаты</a>
            @else
                <a class="auth auth-link blacklink" href="">Войти</a>
                <a class="publish auth-link orangebutton" href="">Добавить сделку</a>
            @endif
        </div>
        <div class="header-left">
            @if (Request::is('/') || Request::is('search') || Request::is('tag/*') || Request::is('category/*'))
                <div class="mob-menu-wrapper">
                    <div class="mob-menu"></div>
                </div>
            @endif
            <a class="logo" href="{{ url('/') }}"></a>
            <div class="location">
            <a href="" class="location-link blacklink">Ингушетия</a>
                {{--<div class="popup location-popup">--}}
                    {{--<div class="popup-close"></div>--}}
                    {{--<div class="popup-tail">--}}
                        {{--<div class="resp-title-wrapper">--}}
                            {{--<div class="resp-title">Местоположение</div>--}}
                        {{--</div>--}}
                        {{--<form action="">--}}
                            {{--<span class="tip2">Выберите регион, где хотите искать сделки</span>--}}
                            {{--<div class="form-item">--}}
                                {{--<input placeholder="Регион ..." class="input-text" name="location" id="location" type="text">--}}
                                {{--<select  name="location" class="input-select" placeholder="Выберите регион..." id="input-locations">--}}
                                    {{--<option value="">Выберите регион...</option>--}}
                                    {{--<option value="Россия">Россия</option>--}}
                                    {{--@foreach($regions as $region)--}}
                                        {{--<option value="{{ $region->title }}"--}}

                                        {{-->{{ $region->title }}</option>--}}
                                    {{--@endforeach--}}
                                {{--</select>--}}
                            {{--</div>--}}
                            {{--<div class="locations">--}}
                                {{--<div class="location-item orangebutton">Республика Ингушетия</div>--}}
                                {{--<div class="location-item orangebutton">Грозный</div>--}}
                                {{--<div class="location-item orangebutton">Назрань</div>--}}
                            {{--</div>--}}
                            {{--<button class="save-location">Сохранить</button>--}}
                        {{--</form>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>

</header>

<div class="content clearfix">
    <div class="wrapper">
        @yield('content')
    </div>
</div>

<div class="popup-auth popup-auth-chat" style="display: none" id="auth">
    <div class="popup-auth-wrapper">
        <div class="popup-close"></div>
        <div class="popup-auth-left">
            <div class="popup_logo"></div>
            <div class="auth-left-signin">
                <h2>Меняйся!</h2>
            </div>
            <div class="auth-left-signup">
                <h2>Присоединяйся к Kerda</h2>
                <span>Обменивайся товарами и услугами</span>
            </div>
        </div>
        <div class="popup-auth-right">
            <div class="popup-signin">
                <div class="popup-auth-title">Вход в аккаунт</div>
                <div class="social_auth clearfix">
                    <a class="facebook-login" href="{{ url('social/facebook') }}"></a>
                    <a class="vk-login" href="{{ url('social/vkontakte') }}"></a>
                    <a class="google-login" href="{{ url('social/google') }}"></a>
                </div>
                <div class="deal-separator">
                    <span>Или</span>
                </div>
                <form class="auth_form" action="{{ route('login') }}" method="post">
                    {{ csrf_field() }}
                    <login v-on:login="auth" :errors="errors"></login>
                    <div class="auth_reg">
                        Нет аккаунта? <a href="" class="orangelink register_link">Зарегистрируйтесь</a>
                    </div>
                </form>
            </div>
            <div class="popup-signup">
                <div class="popup-auth-title">Регистрация</div>
                <div class="social_auth clearfix">
                    <a class="facebook-login" href="{{ url('social/facebook') }}"></a>
                    <a class="vk-login" href="{{ url('social/vkontakte') }}"></a>
                    <a class="google-login" href="{{ url('social/google') }}"></a>
                </div>
                <div class="deal-separator">
                    <span>Или</span>
                </div>
                <form class="auth_form" action="{{ route('register') }}" method="post">
                    {{ csrf_field() }}
                    <register v-on:register="register" :errors="errors"></register>
                    <div class="auth_reg">
                        Уже есть аккаунт? <a href="" class="orangelink signin_link">Войдите</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="footer" style="{{ Request::is('chats/*') ? 'display: none' : '' }}">
        <div class="wrapper">
            <ul class="footer-menu">
                <li><a class="blacklink" href="">Правила сайта</a></li>
                <li><a class="blacklink" href="/about">О проекте</a></li>
            </ul>
            <div class="copyright">© Kerda — первый сайт объявлений и бартерных сделок в Ингушетии.</div>
        </div>
    </div>
    <script>
        var ROOTURL = '{{ url('/') }}';
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script> window.jQuery || document.write('<script src="js/jquery-3.1.0.min.js"><\/script>')</script>
    <script type="text/javascript" src="{{ asset('js/fotorama.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/dropzone.js') }}"></script>
    <script src="{{ version('js/main.js') }}"></script>
    <script src="{{ version('js/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/selectize.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/masonry.pkgd.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/autosize.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/inputmask.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.cookie.js') }}"></script>
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter47544622 = new Ya.Metrika({
                        id:47544622,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true,
                        webvisor:true
                    });
                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/47544622" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->
</body>
</html>