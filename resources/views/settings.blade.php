@extends('layout')

@section ('content')

<div class="content-settings">
    <div class="page-content">
        <h2 class="page-title">Мой профиль</h2>
        <form action="{{ url('settings') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="settings-left">
                <div id="thumbnail" class="display-avatar">
                    <img id="load-image" src="{{ Auth::user()->userAvatarURL }}" alt="">
                </div>
                <label class="upload-avatar">
                    <span class="upload-avatar-button graybutton">Загрузить аватар</span>
                    <input type="file" name="file" accept="image/*" id="upload-image">
                </label>
                <div class="upload-avatar-desc">
                    <span>Макс. размер: 1 Mb</span>
                    <span>Формат: JPG, PNG или GIF</span>
                </div>
            </div>
            <div class="settings-right">
                <div class="settings-form">
                    @if (session('message'))
                        <div style="color: green; margin-bottom: 20px">
                            {{ session('message') }}
                        </div>
                    @endif
                    @if (session('warning'))
                        <div style="color: #ff6602; margin-bottom: 20px">
                            {{ session('warning') }}
                        </div>
                    @endif
                    <div class="form-item">
                        <label class="deal-label" for="name">Имя<span class="important">*</span></label>
                        <input class="input-text" id="name" type="text" name="name" value="{{ Auth::user()->name }}" required>
                    </div>
                    <div class="form-item">
                        <label class="deal-label" for="email">E-mail<span class="important">*</span></label>
                        <input class="input-text" id="email" type="text" name="email" value="{{ Auth::user()->email }}" required>
                    </div>
                    <div class="form-item">
                        <label class="deal-label" for="phone">Телефон</label>
                        <input placeholder="+7 (___) ___-__-__" type="tel" id="phone" class="input-text" name="phone" value="{{ Auth::user()->phone }}" type="text">
                    </div>
                    <div class="form-item">
                        <label class="deal-label" for="city">Город</label>
                        <input class="input-text" id="city" type="text" value="{{ Auth::user()->city }}" name="city">
                    </div>
                    <div class="form-item">
                        <label class="deal-label" for="password">Пароль</label>
                        <input class="input-text" id="password" type="text" name="password">
                    </div>
                    <div class="form-item form-item-checkbox">
                        <input type="checkbox" class="checkbox" id="checkbox" name="receiver" value="1"
                        @if (Auth::user()->receiver) checked @endif />
                        <label for="checkbox">Получать уведомления на почту</label>
                    </div>
                    <button class="orangebutton">Сохранить</button>
                </div>
            </div>
        </form>
    </div>
</div>

@stop