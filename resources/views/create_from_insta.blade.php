@extends('layout')

@section ('content')

    <div class="content-middle">
        <div class="add-deal clearfix" id="edit">
            @if (session('message'))
                <h2>
                    {{ session('message') }}
                </h2>
            @else
                <h2>Добавить объявление на страницу в инстаграм</h2>

                <form class="form-ad-wrapper" action="{{ URL::to('create_insta') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{--@if (Request::is('edit/*'))--}}
                        {{--<delete-images :images="images">--}}
                        {{--</delete-images>--}}
                    {{--@endif--}}
                    {{--<upload-images :images="images">--}}
                    {{--</upload-images>--}}

                    <p class="uploader-label">Фото</p>
                    <div action="{{ Request::root() }}/uploadImages" class="dropzone" id="my-awesome-dropzone"
                         data-img-url="{{ URL::asset('img/image.svg') }}"
                         data-rm-url="{{ Request::root() }}/delete/">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </div>
                    <p class="field-description">Кликните или перетащите сюда до 9 фото</p>

                    <div class="form-item">
                        {{--<label class="deal-label" for="input-desc">Текст</label>--}}
                        <textarea placeholder="Скопируйте сюда текст..." class="input-textarea" name="description" id="input-desc" cols="30" rows="10">{{ Request::is('edit/*') ? $ad->description : '' }}</textarea>
                    </div>
                    <button class="orangebutton">Опубликовать</button>
                </form>
            @endif
        </div>
    </div>
    <div class="image-error">Если изображения не загружаются, нажмите три точки на верху экрана и выберите "Открыть в Chrome".</div>

@stop