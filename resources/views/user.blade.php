@extends('layout')

@section ('content')
    <div id="user">
        <div class="content2-left">
            <div class="widget widget-profile">
                <a href="{{ url('author') }}/{{ $author->id }}" class="avatar"><img src="@if (isset($author)){{ $author->userAvatarURL }} @else {{ Auth::user()->userAvatarURL }} @endif" alt=""></a>
                <a class="author-name blacklink" href="{{ url('author') }}/{{ $author->id }}">@if (isset($author)){{ $author->name }} @else {{ Auth::user()->name }} @endif</a>
                @if (isset($author))
                    <span class="age">На сайте с {{ $author->created_at->format('d.m.Y') }}</span>
                @endif
                @if (!isset($author))
                    <a class="graybutton settings-button" href="{{ url('/settings') }}">Настройки профиля</a>
                @endif
            </div>
            @if (Request::is('user/*'))
                <div class="widget widget-menu">
                    <div class="profile-menu">
                        <ul>
                            <li class="{{ Request::is('user/status=active') ? 'active' : '' }}">
                                <router-link class="blacklink" to="/user/status=active">Активные</router-link>
                            </li>
                            <li class="{{ Request::is('user/status=deactivated') ? 'active' : '' }}">
                                <router-link class="blacklink" to="/user/status=deactivated">Неактивные</router-link>
                            </li>
                            <li class="{{ Request::is('user/status=favorite') ? 'active' : '' }}">
                                <router-link class="blacklink" to="/user/status=favorite">Избранное</router-link>
                            </li>
                        </ul>
                    </div>
                </div>
            @endif
        </div>
        <div class="content2-right">
            <div class="grid-sizer"></div>
            <div class="gutter-sizer"></div>
            <router-view></router-view>
        </div>
    </div>
</div>
@stop

@if (isset($author))
    <script>
        var author_id = {!! $author->id !!}
    </script>
@else
    <script>
        var author_id = null
    </script>
@endif
