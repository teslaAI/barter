@extends('layout')

@section ('content')

    <div class="content-middle">
        <div class="reset-pass clearfix">
            @if (isset($message))
                <h2>{{ $header }}</h2>
                <p>{{ $message }}</p>
            @else
                <h2>Подтверждение email</h2>
                <p>Письмо с инструкциями по подтверждению email отправлено на адрес <b>{{ Request::get('email') }}</b>.<p>
                <p>Если письмо долго не приходит, проверьте папку Спам (папку для нежелательной почты).</p>
            @endif
        </div>
    </div>

@stop