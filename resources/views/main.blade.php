@extends('layout')

@section('meta')
    <meta name="description" content="Cвежие объявления и бартерные сделки от частных лиц и компаний. Самый простой способ совершить обмен товарами и услугами. Разместить объявление или бартерную сделку бесплатно на Kerda.">
    <meta property="og:title" content="Kerda &mdash; сервис обмена товарами и услугами">
    <meta property="og:type" content="website">
    <meta property="og:url" content="{{ Request::url() }}">
    <meta property="image" content="{{ asset('uploads/og-cover.jpg') }}">
    <meta property="og:image" content="{{ asset('uploads/og-cover.jpg') }}">
    <meta property="og:description" content="Cвежие объявления и бартерные сделки от частных лиц и компаний. Самый простой способ совершить обмен товарами и услугами. Разместить объявление или бартерную сделку бесплатно на Kerda.">
@endsection

@section ('content')

    <div class="content-right resp-menu-wrapper clearfix">
        <div class="menu widget">
            <h4 class="menu-title">Категории</h4>
            <ul class="nav">
            @foreach ($categories as $category)
                <li class="nav-item {{ $category->isActiveParentOrChild() ? 'active' : ''}}">
                    @if ($category->adsCount)
                        <p>{{ $category->name }}<span>{{ $category->adsCount }}</span></p>
                        <ul class="submenu">
                            <li class="">
                                <a class="graylink {{ $category->isActive ? 'active' : ''}}" href='{{ URL::to("category/$category->slug") }}'>Все сделки</a>
                            </li>
                            @foreach($category->children as $child)
                                <li class="">
                                    <a class="graylink {{ $child->isActive ? 'active' : ''}}" href='{{ URL::to("category/$child->slug") }}'>
                                        <span class="subcats-count">{{ $child->adsCount }}</span>
                                        {{ $child->name }}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    @endif
                </li>
            @endforeach
            </ul>
        </div>
        <div class="widget topbarter">
            <h4 class="menu-title">Больше всего хотят</h4>
            <div class="card-tags tags">
                @foreach($tags as $tag)
                <a class="tag" href='{{ URL::to("tag/$tag->name") }}'>{{ $tag->name }}<span>{{ $tag->tagsCount }}</span></a>
                @endforeach
            </div>
        </div>
        {{--<div class="right-show">--}}
            {{--<a rel="nofollow" target="_blank" href=""><img src="" alt=""></a>--}}
        {{--</div>--}}
    </div>
    <div class="content-left" id="root">
        @if (Request::is('/') && !Request::get('search'))
            <div class="owl-carousel aggregator">
                <a href="/?search=авто" class="collection"><img src="/uploads/collections/collection-auto.jpg" alt=""><span>Что предлагают за ваш автомобиль?</span></a>
                <a href="/?search=бесплатно" class="collection"><img src="/uploads/collections/collection-presents.jpg" alt=""><span>Что можно взять бесплатно?</span></a>
                <a href="/?search=смартфон" class="collection"><img src="/uploads/collections/collection-smartphones.jpg" alt=""><span>Что предлагают за смартфон?</span></a>
                <a href="/?search=земельный+участок" class="collection"><img src="/uploads/collections/collection-land.jpg" alt=""><span>Что предлагают за земельный участок?</span></a>
            </div>
        @endif
        @include('inc/search')
        <div class="load-wrapper" style="position: relative; height: 100%;">
            <img src="/img/loader.gif" style="text-align: center; top: 50%; position: absolute; left: 48%;"/>
        </div>
        @if($category_name)
            <h2 class="card-list-title">Категория: <span id="highlighted" >{{ $category_name }}</span></h2>
        @endif
        @if($tagName)
            <h2 class="card-list-title">Хотят: <span id="highlighted" >{{ $tagName }}</span></h2>
        @endif
        @if($search_param)
            <h2 class="card-list-title">{{ $search_param == 'tags' ? 'Хотят' : 'Предлагают' }}: <span id="highlighted" >{{ Request::get('search') }}</span></h2>
        @endif
        @if (Request::is('category/*'))
            <h2 class="card-list-title"></h2>
        @endif
        <div class="card-list">
            <div class="grid-sizer"></div>
            <div class="gutter-sizer" ></div>
            @foreach($ads as $ad)
                <div class="card-wrapper">
                    <div class="card clearfix">
                        <a class="card-img" href="{{ url('/show') . '/' . $ad->id }}">
                            <img src="{{ URL::asset($ad->thumbnailUrl) }}" alt="">
                        </a>
                        <div title="Добавить в избранное" @click="favorite({{$ad->id}})" class="favorite {{ $ad->usersFavorite ? 'active' : '' }}">
                            <div v-if="data == 1" class="f-tooltip">Cначала
                                <a href="" class="auth">авторизуйтесь</a>
                            </div>
                        </div>
                        <a class="card-title blacklink" title="{{ $ad->title }}" href="{{ url('/show') . '/' . $ad->id }}">{{ $ad->title }}</a>
                        <div class="card-separator">
                            <span>хочу взамен:</span>
                        </div>
                        <div class="card-tags tags">
                            @foreach($ad->tags as $tag)
                                <a class="tag" href="{{ URL::to("tag/$tag->name") }}">{{ $tag->name }}</a>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endforeach
            <ads-composer :ads="ads" :url="'{{ url('/') }}'"></ads-composer>
        </div>
        <load-ads v-on:loadedads="loadAds"
                      :search="search"
                      :option="option"
                      :ads="ads"
                      :finish="finish"
                      :offset="offset"
                      :category="category"
                      :tag="tag"
                      :param_link="param_link">
        </load-ads>
        @if (!count($ads))
            <div class="load-wrapper">
                <h1 class="no-items">Будь первым</h1>
            </div>
        @endif
    </div>

@stop

<script>
    var ads = {!! $ads !!}
    var offset = '{{ $offset }}'
    var option = '{{ $option }}'
    var search = '{{ $search }}'
    var category = '{{ $cat }}'
    var tag = '{{ $tagName }}'
    var param_link = '{{ $param_link }}'
    var all_ads = {{ $all_ads }}
</script>