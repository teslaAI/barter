@extends('layout')

@section ('content')

    <div class="content-middle">
        <div class="add-deal clearfix" id="edit">
            <h2>{{ Request::is('create') ? 'Добавить' : 'Редактировать' }} сделку</h2>

            <form action="@if (Request::is('edit/*')) {{ URL::to('update').'/'.$ad->id }} @else {{ URL::to('create') }} @endif" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                @if (Request::is('edit/*'))
                    <delete-images :images="images">
                    </delete-images>
                @endif
                <upload-images :images="images">
                </upload-images>
                <div class="form-item">
                    <label class="deal-label" for="title">Заголовок<span class="important">*</span></label>
                    <input required name="title" value="{{ Request::is('edit/*') ? $ad->title : '' }}" placeholder="Введите заголовок..." class="input-text" id="title" type="text">
                </div>
                <div class="form-item">
                    <label class="deal-label" for="input-city">Город<span class="important">*</span></label>
                    {{--<input {{ Request::is('edit/*') ? '' : 'required' }} autocomplete="off" type="text" placeholder="{{ Request::is('edit/*') ? $city : 'Введите свой город...' }}" v-model="keywords" class="input-text search-form-location" list="data">--}}
                    {{--<input name="location" type="hidden" class="search-location-input">--}}
                    {{--<div v-show="results.length">--}}
                        {{--<ul class="search-form-results" id="data" style="left: 0; right: 0; position: inherit">--}}
                            {{--<li v-for="result in results" :key="result.id" :id="result.id" v-html="highlight(result.title) + ' ' + (result.area ? result.area : '')"></li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}

                    <select required name="city_id" class="input-select" placeholder="Выберите город..." id="input-city">
                        <option value="">Выберите город...</option>
                        <optgroup label="Ингушетия">
                            @foreach($cities as $city)
                                <option value="{{ $city->id }}"
                                    @if ( (Request::is('edit/*') && $ad->city_id == $city->id) || Auth::check() && $city->id == Auth::user()->city_id  || $city->name == geoip()->getLocation($ip = null)->city )
                                        selected
                                    @endif
                                >{{ $city->title }}</option>
                            @endforeach
                        </optgroup>
                    </select>
                </div>
                <div class="form-item">
                    <label class="deal-label" for="input-cat">Категория<span class="important">*</span></label>
                    <select required name="category_id" class="input-select" placeholder="Выберите категорию..." id="input-cat">
                        <option value="">Выберите категорию...</option>
                        @foreach($categories as $category)
                            <optgroup label="{{ $category->name }}">
                                @foreach($category->children as $child)
                                    <option value="{{ $child->id }}"
                                        @if ( (Request::is('edit/*') && $ad->category_id == $child->id) )
                                            selected
                                        @endif
                                    >{{ $child->name }}</option>
                                @endforeach
                            </optgroup>
                        @endforeach
                    </select>
                </div>
                <div class="form-item">
                    <label class="deal-label" for="phone">Телефон</label>
                    <input placeholder="+7 (___) ___-__-__" id="phone" type="tel" class="input-text" name="phone" value="@if ( (Request::is('edit/*') && $ad->phone)) {{ $ad->phone }} @else {{ Auth::user()->phone }} @endif" type="text">
                </div>
                <div class="form-item">
                    <label class="deal-label" for="input-desc">Описание</label>
                    <textarea placeholder="Введите описание..." class="input-textarea" name="description" id="input-desc" cols="30" rows="10">{{ Request::is('edit/*') ? $ad->description : '' }}</textarea>
                </div>
                <div class="form-item">
                    <label class="deal-label" for="input-options">Хочу взамен</label>
                    <span class="tip">(выберите из списка или добавьте свой вариант)</span>
                    <select multiple name="tags[]" class="input-select" placeholder="Введите то, что хотите взамен..." id="input-options">
                        <option value="">Введите то, что хотите взамен...</option>
                        @foreach($tags as $tag)
                            <option value="{{ $tag->id }}"
                                @if ( (Request::is('edit/*') && in_array($tag->id, $ad->tagList)))
                                    selected
                                @endif
                            >{{ $tag->name }}</option>
                        @endforeach
                    </select>
                    <span class="tip3">Слишком длинное название</span>
                </div>
                <button onclick="removeSpace()" class="orangebutton">Опубликовать</button>
            </form>
        </div>
    </div>

@stop

@if (Request::is('edit/*'))
    <script>
        var ad_id = '{{$ad->id}}'
    </script>
@else
    <script>
        var ad_id = null
    </script>
@endif