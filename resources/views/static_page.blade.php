@extends('layout')

@section ('content')

    <div class="content-static">
        <div class="page-content">
            <div class="content-static-cover about">
            </div>
            <h2 class="page-title">О проекте</h2>
            <div class="content-static-text">
                <p>Kerda - это платформа для размещения объявлений и сервис, который поможет вам легко обмениваться товарами и услугами. Уникальный алгоритм сайта позволит быстро найти то, на что вы хотите обменятся.</p>
                <p>По всем вопросам и предложениям пишите на <a class="orangelink" href="mailto:info@brtr.ru">info@brtr.ru</a></p>
            </div>
        </div>
    </div>

@stop