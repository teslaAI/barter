<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Регистрация на Kerda</h2>

<div>
    Почти готово!
    Здравствуйте, {{ $username }}!
    Остался последний шаг! Перейдите по ссылке ниже, чтобы подтвердить ваш новый адрес электронной почты, зарегистрированный в Kerda.<br/>

    {{ URL::to('register/verify/' . $confirmation_code) }}<br/>

</div>



</body>
</html>