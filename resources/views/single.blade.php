@extends('layout')

@section('meta')
    <meta name="description" content="{{ $ad->description }}">
    <meta property="og:title" content="{{ $ad->pageTitle }}">
    <meta property="og:type" content="website">
    <meta property="og:url" content="{{  Request::url() }}">
    <meta property="image" content="{{ count($ad->images) ? asset('uploads/deals/'.$ad->images->first()->title) : asset('uploads/placeholder.jpg') }}">
    <meta property="og:image" content="{{ count($ad->images) ? asset('uploads/deals/'.$ad->images->first()->title) : asset('uploads/placeholder.jpg') }}">
    <meta property="og:description" content="{{ $ad->description }}">
@endsection

@section('title', $ad->pageTitle)

@section ('content')

<div class="content-left" id="root">
    @include('inc/search')
    <div class="breadcrumbs">
        <ul class="breadcrumbs_list">
            <li class="breadcrumbs_home"><a class="graylink" href="{{ url('/') }}">Главная</a></li>
            <li><a class="graylink" href="{{ URL::to("category") . '/' . $ad->category->parent->slug }}">{{ $ad->category->parent->name }}</a></li>
            <li><a class="graylink" href="{{ URL::to("category") . '/' . $ad->category->slug }}">{{ $ad->category->name }}</a></li>
        </ul>
    </div>
    <div class="main-content">
        <div class="left-main clearfix">
            @if (count($ad->images))
                <div class="fotorama" data-width="100%" data-ratio="4/3" data-nav="thumbs" data-arrows="true"  data-allowfullscreen="true" data-thumbwidth="112" data-thumbheight="84" data-thumbmargin="15" data-thumbborderwidth="0"
                >
                    @foreach($ad->images as $image)
                        <img src="{{ asset('uploads/deals/'.$image->title) }}">
                    @endforeach
                </div>
            @else
                <div class="fotorama" data-width="100%" data-ratio="4/3" data-arrows="true" data-nav="thumbs" data-allowfullscreen="true" data-thumbwidth="112" data-thumbheight="84" data-thumbmargin="15" data-thumbborderwidth="0"
                >
                <img src="{{ asset('uploads/placeholder.jpg') }}" alt="">
                </div>
            @endif
            {{--<div class="watching">Этот товар смотрят 16 человек</div>--}}
            <div class="watching">Просмотры: {{ $ad->views }}</div>
            <div class="date">Размещено {{ LocalizedCarbon::instance($ad->created_at)->diffForHumans() }}</div>
        </div>
        <div class="right-main clearfix">
            @if (Auth::user() && $user->id == Auth::user()->id)
                <div class="edit-link-wrapper">
                    <a class="blacklink edit-link" href="{{ url('edit', $ad->id) }}">Изменить</a>
                </div>
            @endif
            <h1>{{ $ad->title }}</h1>
            <div class="desc">
                {!! nl2p(htmlentities($ad->description)) !!}
            </div>
            <div class="deal-separator">
                <span>хочу взамен:</span>
            </div>
            <div class="tags">
                @foreach($ad->tags as $tag)
                    <a class="tag" href="{{ URL::to("tag/$tag->name") }}">{{ $tag->name }}</a>
                @endforeach
            </div>
            @if ($ad->phone)
                <a class="phone-btn" href="">Показать телефон</a>
            @endif
            @if (Auth::user() && $user->id != Auth::user()->id && $user->id != 4 && $user->id != 5)
                <a class="chat-btn" href="">Чат с автором</a>
            @elseif (!Auth::user())
                <a class="auth-chat" href="">Чат с автором</a>
            @endif
            <div class="share">
            <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
            <script src="//yastatic.net/share2/share.js"></script>
            <div class="ya-share2" data-services="vkontakte,facebook,whatsapp,telegram"></div>
            </div>
            <div title="Добавить в избранное" @click="favorite({{$ad->id}})" class="favorite {{ $ad->usersFavorite ? 'active' : '' }}"></div>
        </div>
    </div>
</div>
<div class="content-right clearfix">
    <div class="widget author-widget">
        <h4 class="menu-title">Сделки автора</h4>
        <a href="{{ url('author', $user->id) }}" class="avatar">
            <img src="{{ $user->userAvatarURL }}" alt="">
        </a>
        <a class="author-name blacklink" href="{{ url('author', $user->id) }}">{{ $user->name }}</a>
        <span class="age">На сайте с {{ $user->created_at->format('d.m.Y') }}</span>
        <div class="author-offers">
            @foreach($userAds as $userAd)
                <div class="author-offers-item">
                    <a href="{{ action('AdController@show', $userAd->id) }}">
                        <img src="{{ URL::asset($userAd->thumbnailUrl) }}" alt="">
                    </a>
                    <a class="author-offers-item-title blacklink" href="{{ $userAd->id }}">{{ $userAd->title }}</a>
                </div>
            @endforeach
        </div>
        <a class="graybutton" href="{{ url('author', $user->id) }}">Смотреть все</a>
    </div>
</div>

</div>

<div class="popup-auth popup-show-phone">
    <div class="popup-auth-wrapper">
        <div class="popup-close"></div>
        <div class="author-widget">
            <a href="{{ url('author', $user->id) }}" class="avatar">
                <img src="{{ $user->userAvatarURL }}" alt="">
            </a>
            <a class="author-name blacklink" href="">{{ $user->name }}</a>
            <span class="age">На сайте с {{ $user->created_at->format('d.m.Y') }}</span>
        </div>
        <div class="show-phone">
            <span class="author-phone-city">{{ $ad->city->title }}</span>
            <a class="author-phone blacklink" href="tel:+7{{ $ad->phone }}">+7{{ $ad->phone }}</a>
            <span class="author-phone-notice">Скажите, что Вы нашли эту сделку на Kerda.ru</span>
        </div>
    </div>
</div>

<div class="popup-auth popup-show-chat">
    <div class="popup-auth-wrapper">
        <div class="popup-close"></div>
        <a href="{{ url('author', $user->id) }}" class="avatar">
            <img src="{{ $user->userAvatarURL }}" alt="">
        </a>
        <a class="author-name blacklink" href="{{ url('author', $user->id) }}">{{ $user->name }}</a>
        <span class="age">На сайте с {{ $user->created_at->format('d.m.Y') }}</span>
        <form action="{{ url('chats') . '/all/' . $ad->id . '/' . $user->id }}" method="post" class="show-chat">
            {{ csrf_field() }}
            <textarea required type="text" class="input-textarea" name="message" placeholder="введите сообщение..."></textarea>
            <button class="orangebutton">Отправить</button>
        </form>
    </div>
</div>

@stop