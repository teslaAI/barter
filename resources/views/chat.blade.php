@extends('layout')

@section('content')
    <div id="chat-router">
        <div class="content2-left">
            <div class="widget widget-menu">
                <div class="profile-menu">
                    <ul>
                        <li class="@if (Request::is('*/all')  ||  Request::is('*/all/*')) active @endif">
                            <router-link class="blacklink" to="/chats/all">Все сделки</router-link>
                        </li>
                        <li class="@if (Request::is('*/proposes')  ||  Request::is('*/proposes/*')) active @endif">
                            <router-link class="blacklink" to="/chats/proposes">Мне предлагают</router-link>
                        </li>
                        <li class="@if (Request::is('*/offers')  ||  Request::is('*/offers/*')) active @endif">
                            <router-link class="blacklink" to="/chats/offers">Я предлагаю</router-link>
                        </li>
                        {{--<li class="@if (Request::is('*/blocked')  ||  Request::is('*/blocked/*')) active @endif"><a class="blacklink" href="{{ url('chats/blocked') }}">Заблокированные</a></li>--}}
                    </ul>
                </div>
            </div>
        </div>
        <div class="content2-right">
            <router-view :url="'{{ url('/') }}'" :auth_user="{{ Auth::user()->id }}"></router-view>
        </div>
    </div>
@stop