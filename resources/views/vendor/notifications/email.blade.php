@component('mail::message')
{{-- Greeting --}}
@if (! empty($greeting))
# {{ $greeting }}
@else
@if ($level == 'error')
# Whoops!
@else
# Здравствуйте!
@endif
@endif

{{-- Intro Lines --}}
<p>Вы получили это письмо, так как сделали запрос на изменение пароля на сайте brtr.ru.</p>

{{-- Action Button --}}
@isset($actionText)
<?php
    switch ($level) {
        case 'success':
            $color = 'green';
            break;
        case 'error':
            $color = 'red';
            break;
        default:
            $color = 'blue';
    }
?>
@component('mail::button', ['url' => $actionUrl, 'color' => $color])
Сбросить пароль
@endcomponent
@endisset

{{-- Outro Lines --}}
<p>Если вы не давали запрос на изменение пароля, то игнорируйте данное сообщение.</p>

{{-- Salutation --}}
@if (! empty($salutation))
{{ $salutation }}
@else
С наилучшими пожеланиями,<br>{{ config('app.name') }}
@endif

{{-- Subcopy --}}
@isset($actionText)
@component('mail::subcopy')
Если у вас возникли проблемы при нажатии кнопки сброса пароля, скопируйте данную ссылку в браузере: [{{ $actionUrl }}]({{ $actionUrl }})
@endcomponent
@endisset
@endcomponent
