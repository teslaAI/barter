@extends('layout')

@section('content')
    <div class="content-middle">
        <div class="reset-pass clearfix">
            <h2>Сбросить пароль</h2>
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            <form role="form" method="POST" action="{{ route('password.email') }}">
                {{ csrf_field() }}
                <div class="form-item">
                    <label for="email" class="deal-label">E-Mail адрес</label>
                    <input id="email" type="email" placeholder="Введите ваш E-mail" class="input-text" name="email" value="{{ old('email') }}" required>
                    @if ($errors->has('email'))
                        <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                    @endif
                </div>
                <button type="submit" class="orangebutton">Сбросить</button>
            </form>
        </div>
    </div>
@endsection
