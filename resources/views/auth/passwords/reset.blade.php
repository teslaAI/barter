@extends('layout')

@section ('content')

    <div class="content-middle">
        <div class="reset-pass clearfix">
            <h2>Сброс пароля</h2>
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                <form role="form" method="POST" action="{{ route('password.request') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="token" value="{{ $token }}">
                    <div class="form-item">
                        <label for="email" class="deal-label">E-Mail адрес</label>
                        <input id="email" placeholder="Введите ваш E-mail" type="email" class="input-text" name="email" value="{{ $email or old('email') }}" required autofocus>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-item">
                        <label for="password" class="deal-label">Пароль</label>
                        <input id="password" placeholder="Введите новый пароль" type="password" class="input-text" name="password" required>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-item">
                        <label for="password-confirm" class="deal-label">Подтверждение пароля</label>
                        <input id="password-confirm" placeholder="Введите пароль еще раз" type="password" class="input-text" name="password_confirmation" required>
                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                        @endif
                    </div>
                    <button type="submit" class="orangebutton">Отправить</button>
                </form>
        </div>
    </div>
@stop
