<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        DB::table('users')->truncate();

        DB::table('users')->insert([
            [
                'name' => 'Ахмед',
                'email' => 'storona77@gmail.com',
                'confirmed' => true,
                'password' => bcrypt(111111)
            ],[
                'name' => 'Тимур',
                'email' => 'a@gmail.com',
                'confirmed' => true,
                'password' => bcrypt(111111)
            ],[
                'name' => 'Осик',
                'email' => 'b@gmail.com',
                'confirmed' => true,
                'password' => bcrypt(111111)
            ],
        ]);
    }
}
