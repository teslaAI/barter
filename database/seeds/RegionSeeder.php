<?php

use Illuminate\Database\Seeder;

class RegionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vk_regions')->delete();

        DB::table('vk_regions')->insert([
            [
                "title" => "Адыгея",
                "country_id" => 1
            ], [
                "title" => "Алтай",
                "country_id" => 1
            ], [
                "title" => "Алтайский край",
                "country_id" => 1
            ], [
                "title" => "Амурская область",
                "country_id" => 1
            ], [
                "title" => "Архангельская область",
                "country_id" => 1
            ], [
                "title" => "Астраханская область",
                "country_id" => 1
            ], [
                "title" => "Башкортостан",
                "country_id" => 1
            ], [
                "title" => "Белгородская область",
                "country_id" => 1
            ], [
                "title" => "Брянская область",
                "country_id" => 1
            ], [
                "title" => "Бурятия",
                "country_id" => 1
            ], [
                "title" => "Владимирская область",
                "country_id" => 1
            ], [
                "title" => "Волгоградская область",
                "country_id" => 1
            ], [
                "title" => "Вологодская область",
                "country_id" => 1
            ], [
                "title" => "Воронежская область",
                "country_id" => 1
            ], [
                "title" => "Дагестан",
                "country_id" => 1
            ], [
                "title" => "Еврейская АОбл",
                "country_id" => 1
            ], [
                "title" => "Забайкальский край",
                "country_id" => 1
            ], [
                "title" => "Ивановская область",
                "country_id" => 1
            ], [
                "title" => "Ингушетия",
                "country_id" => 1
            ], [
                "title" => "Иркутская область",
                "country_id" => 1
            ], [
                "title" => "Кабардино-Балкарская",
                "country_id" => 1
            ], [
                "title" => "Калининградская область",
                "country_id" => 1
            ], [
                "title" => "Калмыкия",
                "country_id" => 1
            ], [
                "title" => "Калужская область",
                "country_id" => 1
            ], [
                "title" => "Камчатский край",
                "country_id" => 1
            ], [
                "title" => "Карачаево-Черкесская",
                "country_id" => 1
            ], [
                "title" => "Карелия",
                "country_id" => 1
            ], [
                "title" => "Кемеровская область",
                "country_id" => 1
            ], [
                "title" => "Кировская область",
                "country_id" => 1
            ], [
                "title" => "Коми",
                "country_id" => 1
            ], [
                "title" => "Корякский АО",
                "country_id" => 1
            ], [
                "title" => "Костромская область",
                "country_id" => 1
            ], [
                "title" => "Краснодарский край",
                "country_id" => 1
            ], [
                "title" => "Красноярский край",
                "country_id" => 1
            ], [
                "title" => "Курганская область",
                "country_id" => 1
            ], [
                "title" => "Курская область",
                "country_id" => 1
            ], [
                "title" => "Ленинградская область",
                "country_id" => 1
            ], [
                "title" => "Липецкая область",
                "country_id" => 1
            ], [
                "title" => "Магаданская область",
                "country_id" => 1
            ], [
                "title" => "Марий Эл",
                "country_id" => 1
            ], [
                "title" => "Мордовия",
                "country_id" => 1
            ], [
                "title" => "Московская область",
                "country_id" => 1
            ], [
                "title" => "Мурманская область",
                "country_id" => 1
            ], [
                "title" => "Ненецкий АО",
                "country_id" => 1
            ], [
                "title" => "Нижегородская область",
                "country_id" => 1
            ], [
                "title" => "Новгородская область",
                "country_id" => 1
            ], [
                "title" => "Новосибирская область",
                "country_id" => 1
            ], [
                "title" => "Омская область",
                "country_id" => 1
            ], [
                "title" => "Оренбургская область",
                "country_id" => 1
            ], [
                "title" => "Орловская область",
                "country_id" => 1
            ], [
                "title" => "Пензенская область",
                "country_id" => 1
            ], [
                "title" => "Пермский край",
                "country_id" => 1
            ], [
                "title" => "Приморский край",
                "country_id" => 1
            ], [
                "title" => "Псковская область",
                "country_id" => 1
            ], [
                "title" => "Ростовская область",
                "country_id" => 1
            ], [
                "title" => "Рязанская область",
                "country_id" => 1
            ], [
                "title" => "Самарская область",
                "country_id" => 1
            ], [
                "title" => "Саратовская область",
                "country_id" => 1
            ], [
                "title" => "Саха /Якутия/",
                "country_id" => 1
            ], [
                "title" => "Сахалинская область",
                "country_id" => 1
            ], [
                "title" => "Свердловская область",
                "country_id" => 1
            ], [
                "title" => "Северная Осетия - Алания",
                "country_id" => 1
            ], [
                "title" => "Смоленская область",
                "country_id" => 1
            ], [
                "title" => "Ставропольский край",
                "country_id" => 1
            ], [
                "title" => "Таймырский (Долгано-Ненецкий) АО",
                "country_id" => 1
            ], [
                "title" => "Тамбовская область",
                "country_id" => 1
            ], [
                "title" => "Татарстан",
                "country_id" => 1
            ], [
                "title" => "Тверская область",
                "country_id" => 1
            ], [
                "title" => "Томская область",
                "country_id" => 1
            ], [
                "title" => "Тульская область",
                "country_id" => 1
            ], [
                "title" => "Тыва",
                "country_id" => 1
            ], [
                "title" => "Тюменская область",
                "country_id" => 1
            ], [
                "title" => "Удмуртская",
                "country_id" => 1
            ], [
                "title" => "Ульяновская область",
                "country_id" => 1
            ], [
                "title" => "Хабаровский край",
                "country_id" => 1
            ], [
                "title" => "Хакасия",
                "country_id" => 1
            ], [
                "title" => "Ханты-Мансийский Автономный округ - Югра АО",
                "country_id" => 1
            ], [
                "title" => "Челябинская область",
                "country_id" => 1
            ], [
                "title" => "Чеченская",
                "country_id" => 1
            ], [
                "title" => "Чувашская",
                "country_id" => 1
            ], [
                "title" => "Чукотский АО",
                "country_id" => 1
            ], [
                "title" => "Ямало-Ненецкий АО",
                "country_id" => 1
            ], [
                "title" => "Ярославская область",
                "country_id" => 1
            ]
        ]);
    }
}
