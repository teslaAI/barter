<?php

use Illuminate\Database\Seeder;

class CitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vk_cities')->delete();

        DB::table('vk_cities')->insert([
            [
                "title" => "36 Участок",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Аки-Юрт",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Али-Юрт",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Алкун",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Алхасты",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Армхи",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Аршты",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Ассиновская",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Бейни",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Бековичи",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Берда-Юрт",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Бишти",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Вежарий-Юрт",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Верхние Ачалуки",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Верхний Алкун",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Вознесенская",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Гази-Юрт",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Гайрбек-Юрт",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Галашки",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Гиреевский",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Гули",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Даттых",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Джейрах",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Долаково",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Зязиков-Юрт",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Инарки",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Кантышево",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Карабулак",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Лейми",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Ляжги",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Магас",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Малгобек",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Мужичи",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Назрань",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Нестеровская",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Нижние Ачалуки",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Новый Редант",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Ольгети",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Орджоникидзевская",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Пседах",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Пялинг",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Сагопши",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Серноводск",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Средние Ачалуки",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Сунжа (Орджоникидзевская)",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Сурхахи",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Троицкая",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Чемульга",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Чкалово",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Эгикал",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Экажево",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Южное",
                "region_id" => "1030371",
                "country_id" => 1
            ], [
                "title" => "Яндаре",
                "region_id" => "1030371",
                "country_id" => 1
            ]
        ]);
    }
}
