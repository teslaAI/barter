<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class AdSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ads')->delete();
        DB::table('ads')->truncate();

        $faker = Faker::create();

        $text = $faker->paragraph();

        DB::table('ads')->insert([[
            'title' => 'Ford Mondeo 1952 года в отличном состоянии',
            'description' => $text,
            'category_id' => 1,
            'user_id' => 1,
            'city_id' => 1,

            'created_at' => $faker->dateTimeThisMonth(),
            'updated_at' => $faker->dateTimeThisMonth(),
        ],[
            'title' => 'Ford Mondeo 1952 года',
            'description' => $text,
            'category_id' => 1,
            'user_id' => 1,
            'city_id' => 1,

            'created_at' => $faker->dateTimeThisMonth(),
            'updated_at' => $faker->dateTimeThisMonth(),
        ],[
        'title' => 'Ford Mondeo 1952 года',
            'description' => $text,
            'category_id' => 1,
            'user_id' => 1,
            'city_id' => 1,

            'created_at' => $faker->dateTimeThisMonth(),
            'updated_at' => $faker->dateTimeThisMonth(),
        ],[
            'title' => 'Ford Mondeo 1952 года',
            'description' => $text,
            'category_id' => 1,
            'user_id' => 1,
            'city_id' => 1,

            'created_at' => $faker->dateTimeThisMonth(),
            'updated_at' => $faker->dateTimeThisMonth(),
        ],[
            'title' => 'Ford Mondeo 1952 года',
            'description' => $text,
            'category_id' => 1,
            'user_id' => 1,
            'city_id' => 1,

            'created_at' => $faker->dateTimeThisMonth(),
            'updated_at' => $faker->dateTimeThisMonth(),
        ],[
            'title' => 'Ford Mondeo 1952 года',
            'description' => $text,
            'category_id' => 1,
            'user_id' => 1,
            'city_id' => 1,

            'created_at' => $faker->dateTimeThisMonth(),
            'updated_at' => $faker->dateTimeThisMonth(),
        ],[
            'title' => 'Ford Mondeo 1952 года',
            'description' => $text,
            'category_id' => 1,
            'user_id' => 1,
            'city_id' => 1,

            'created_at' => $faker->dateTimeThisMonth(),
            'updated_at' => $faker->dateTimeThisMonth(),
        ],[
            'title' => 'Ford Mondeo 1952 года',
            'description' => $text,
            'category_id' => 1,
            'user_id' => 1,
            'city_id' => 1,

            'created_at' => $faker->dateTimeThisMonth(),
            'updated_at' => $faker->dateTimeThisMonth(),
        ],[
            'title' => 'Ford Mondeo 1952 года',
            'description' => $text,
            'category_id' => 1,
            'user_id' => 1,
            'city_id' => 1,

            'created_at' => $faker->dateTimeThisMonth(),
            'updated_at' => $faker->dateTimeThisMonth(),
        ],[
            'title' => 'Ford Mondeo 1952 года',
            'description' => $text,
            'category_id' => 1,
            'user_id' => 1,
            'city_id' => 1,

            'created_at' => $faker->dateTimeThisMonth(),
            'updated_at' => $faker->dateTimeThisMonth(),
        ]
        ]);
    }
}
