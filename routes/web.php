<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'AdController@index');
Route::post('/', 'AdController@index');

Route::get('about', function () { return view('static_page'); });
Route::get('rules', function () { return view('static_page'); });

Route::get('/search', 'AdController@search');
Route::get('/location', 'AdController@apiLocation');

Route::get('/create', 'AdController@create')->middleware('auth');
Route::post('/create', 'AdController@store')->middleware('auth');

Route::get('/create_insta', function () { return view('create_from_insta'); });
Route::post('/create_insta', 'AdController@storeAdFromInstagram');

Route::get('/edit/{id}', 'AdController@edit')->middleware('auth');
Route::post('/edit/{id}', 'AdController@edit')->middleware('auth');
Route::post('/update/{id}', 'AdController@update')->middleware('auth');
Route::post('/destroy/{id}', 'AdController@destroy')->middleware('auth');

Route::post('/toggleactive/{id}', 'AdController@toggleActiveAd')->middleware('auth');

Route::get('/show/{id}', 'AdController@show');

Route::post('uploadImages/', 'AdController@uploadImages');
Route::post('uploadFiles/', 'ChatController@uploadFiles')->middleware('auth');
Route::post('delete/{image}', 'AdController@deleteImages');
Route::post('remove/{file}', 'ChatController@deleteFiles')->middleware('auth');

Route::get('user', 'AdController@userAds')->middleware('auth');
Route::post('user', 'AdController@userAds')->middleware('auth');
Route::get('settings', 'UserController@settings')->middleware('auth');
Route::post('settings', 'UserController@storeSettings')->middleware('auth');

Route::get('author/{id}', 'AdController@authorAds');
Route::post('author/{id}', 'AdController@authorAds');

Route::post('user?status=deactivated', 'AdController@userAds')->middleware('auth');

Route::get('user/status=deactivated', 'AdController@userAds')->middleware('auth');
Route::get('user/status=active', 'AdController@userAds')->middleware('auth');
Route::get('user/status=favorite', 'AdController@userAds')->middleware('auth');


Route::post('favorite/{id}', 'AdController@setRemovefavorite')->middleware('auth');

Route::get('category/{slug}', 'AdController@index');
Route::get('tag/{slug}', 'AdController@index');

Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/confirmation', function () { return view('confirm'); });

Route::get('register/verify/{confirmationCode}', [
    'as' => 'confirmation_path',
    'uses' => 'Auth\RegisterController@confirm'
]);

Route::get('social/{provider}', 'SocialController@redirectToProvider');
Route::get('social/callback/{provider}', 'SocialController@handleProviderCallback');


// Chat routes
Route::get('chats/{parameter}', function () { return view('chat'); });
Route::get('chats/{parameter}/{id}/{author}', function () { return view('chat'); });

Route::post('chats/{parameter}', 'ChatController@chat')->middleware('auth');
Route::post('chats/{parameter}/{id}/{author}', 'ChatController@chat')->middleware('auth');
Route::get('/messages', 'ChatController@getMessages')->middleware('auth');
Route::post('/messages', 'ChatController@createMessage')->middleware('auth');
Route::post('destroy_chat/{id}', 'ChatController@destroyChat')->middleware('auth');
