<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    public $timestamps = false;
    protected $fillable = ['ad_id', 'user_id', 'ad_title', 'ad_image'];

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function ads()
    {
        return $this->belongsToMany(Ad::class);
    }

    public function messages()
    {
        return $this->hasMany(Message::class)->orderBy('created_at', 'desc');
    }

    public function not_viewed_messages()
    {
        return $this->hasMany(Message::class)->where('is_viewed', 0)->where('user_id', '!=', Auth::user()->id)->orderBy('created_at', 'desc');
    }
}
