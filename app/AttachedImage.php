<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttachedImage extends Model
{
    protected $fillable = ['message_id', 'name'];
}
