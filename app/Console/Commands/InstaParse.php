<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\InstaService;

class InstaParse extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'instagram:parse';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'parse ads to instagram';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @var $service
     * @return mixed
     */
    public function handle(InstaService $service)
    {
        $service->uploadTextAndImage('public/img/noavatar.jpg', 'прост');
    }
}
