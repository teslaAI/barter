<?php

namespace App;

use Cache;
use Carbon\Carbon;
use GuzzleHttp\Psr7\Request;
use Illuminate\Database\Eloquent\Model;
use App\Ad;

class Tag extends Model
{
    protected $fillable = ['ad_id', 'tag_id', 'name'];
    protected $appends = ['tagsCount'];

    public function ads()
    {
    	return $this->belongsToMany(Ad::class);
    }

    public static function topTags($region, $cities)
    {
        $minutes = Carbon::now()->addMinutes(10);

        $tags = Cache::remember('tags-' . $region, $minutes, function () use ($cities) {

            $activeAds = function ($query) use ($cities) {
                $query->where('ads.active', 1);
//                    ->whereIn('ads.city_id', [865]);
            };

            return $tags = Tag::withCount(['ads' => $activeAds])
                            ->has('ads')
                            ->orderBy('ads_count', 'desc')
                            ->where('id', '!=', 1)
                            ->limit(20)->get();
        });

        return $tags;
    }

    public function getTagsCountAttribute()
    {
        return $this->ads()->where('active', 1)->count();
    }

    public static function syncTags(Ad $ad, $request)
    {
        $syncTagsList = self::checkForNewTags($request);

        $ad->tags()->sync($syncTagsList);
    }

    public static function checkForNewTags($request)
    {
        if (!$request['tags']) {
            $tags = ["name" => "рассмотрю варианты"];
        } else {
            $tags = array_map('mb_strtolower', $request['tags']);
        }

        $allDBTagsByID = Tag::pluck("id")->toArray();
        $allDBTagsByName = Tag::pluck("name")->toArray();

        $mergedTags = array_merge($allDBTagsByID, $allDBTagsByName);
        $newTagsList = array_diff($tags, $mergedTags);
        $syncTagsList = array_diff($tags, $newTagsList);

        $getOldTagsIds = Tag::whereIn('name', $syncTagsList)->orWhereIn('id', $syncTagsList)->pluck('id');

        foreach ($newTagsList as $newTag)
        {
            if (is_numeric($newTag)) {
                $newTag = number_format($newTag, 0, '.', ' ') . ' руб';
            }

            $newTagModel = Tag::create(["name" => $newTag]);
            $getOldTagsIds[] = $newTagModel->id;
        }

        return $getOldTagsIds;
    }
}
