<?php

namespace App\Observers;

use App\Ad;

class AdObserver
{
    /**
     * Listen to the User created event.
     *
     * @param  \App\Ad  $ad
     * @return void
     */
    public function created(Ad $ad)
    {
        //
    }

    /**
     * Listen to the User deleting event.
     *
     * @param  \App\Ad  $ad
     * @return void
     */
    public function deleting(Ad  $ad)
    {
        //
    }
}