<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Request;


class User extends Authenticatable
{
    use Notifiable;

    protected $appends = ['userAvatarURL'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'phone', 'city', 'password', 'social_id', 'receiver', 'avatar_url', 'confirmed', 'confirmation_code'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function ads()
    {
        return $this->hasMany(Ad::class);
    }

    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    public function chats()
    {
        return $this->belongsToMany(Chat::class);
    }

    public function avatar()
    {
        return $this->hasOne('App\Avatar');
    }

    /**
     * Get the user's avatar URL.
     *
     * @return string
     */
    public function getUserAvatarURLAttribute()
    {
        if ($this->avatar_url)
        {
            return $result = $this->avatar_url;
        }
        elseif ($this->avatar)
        {
            $name = $this->avatar->name;

            $result = "/uploads/avatars/$name";
        }
        else
        {
            $result = "/img/noavatar.jpg";
        }

        return Request::root() . $result;
    }
}
