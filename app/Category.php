<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Request;

class Category extends Model
{
    protected $appends = ['adsCount', 'isActive'];

    public function ads()
    {
    	return $this->hasMany(Ad::class);
    }

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    /**
     * Get all categories with children ordered by priority
     *
     * @param $main
     * @return collection
     */
    public static function getAll($main = null)
    {
        $categories = Category::with(['children' => function($query) use ($main) {
            $query->orderBy('priority', 'desc');

            if ($main) {
                $query->has('ads');
            }

        }])->where('parent_id', null)->orderBy('priority')->get();

        return $categories;
    }

    /**
     * Check if the category is active.
     *
     * @return boolean
     */

    public function getIsActiveAttribute()
    {
        return $this->slug === Request::segment(2);
    }


    /**
     * Check if the category or its children are active.
     *
     * @return boolean
     */
    public function isActiveParentOrChild()
    {
        if ($this->getIsActiveAttribute()) {
            return true;
        }

        foreach ($this->children as $child) {
            if ($child->getIsActiveAttribute()) {
                return true;
            }
        }

        return false;
    }


    /**
     * Get related to the category ad's and count them.
     *
     * @return integer
     */

    public function getAdsCountAttribute()
    {
        if ($this->parent_id)
        {
            return $this->ads()->where('active', 1)->count();
        }
        else
        {
            return $this->hasManyThrough(Ad::class, Category::class, 'parent_id')->where('active', 1)->count();
        }
    }
}
