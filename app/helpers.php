<?php

if (! function_exists('version')) {
    /**
     * Asset file with version parameter.
     *
     * @param  string  $path
     * @return string
     */
    function version($path)
    {
        $version = filemtime(public_path($path));
        return asset("$path?v=$version");
    }
}

if (! function_exists('nl2p')) {
    function nl2p($text)
    {
        $exploded = explode("\r\n", $text);
        $result = '';

        foreach ($exploded as $piece) {
            if ($piece) {
                $result .= "<p>$piece</p>";
            }
        }

        return $result;
    }
}