<?php

namespace App\Policies;

use App\User;
use App\Ad;
use Illuminate\Auth\Access\HandlesAuthorization;

class AdPolicy
{
    /**
     * Determine if the given post can be updated by the user.
     *
     * @param  \App\User  $user
     * @param  \App\Ad  $ad
     * @return bool
     */

    public function before ($user, $ability)
    {
        if ($user->is_admin) {
            return true;
        }
    }

    public function update (User $user, Ad $ad)
    {
        return $user->id === $ad->user_id;
    }
}
