<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\InstaService;
use App\Ad;
use App\Observers\AdObserver;

class InstagramServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(InstaService::class, function () {
            return new InstaService(config('services.instagram'));
        });
    }
}
