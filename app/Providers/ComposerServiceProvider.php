<?php

namespace App\Providers;

use App\Ad;
use Auth;
use Bigperson\VkGeo\Models\Region;
use Cookie;
use Illuminate\Http\Request;
use App\Http\Controllers\ChatController;
use Illuminate\Support\ServiceProvider;
use App\Message;
use App\Chat;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    public function boot(Request $request)
    {
        view()->composer('layout', function ($view) use ($request) {
            $user = Auth::user();
//            $regions = Region::all();
//
//            $geo_location = Ad::location($request);
//
//            $view->with([
//                'geo_location' => $geo_location,
//                'regions' => $regions
//            ]);

            if ($user) {
                $chats = Chat::whereHas('users', function ($query) use ($user) {
                    $query->where('user_id', $user->id);
                })->pluck('id');

                $messages = Message::where('is_viewed', 0)
                    ->whereIn('chat_id', $chats)
                    ->where('user_id', '!=', $user->id)->get();
                $view->with([
                    'messages' => count($messages),
                ]);
            }
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
