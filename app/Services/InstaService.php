<?php

namespace App\Services;

class InstaService
{
    protected $ig = null;

    /**
    * Constructor
    *
    * @var array $config
    */
    public function __construct($config)
    {
        \InstagramAPI\Instagram::$allowDangerousWebUsageAtMyOwnRisk = true;
        $this->ig = new \InstagramAPI\Instagram($config['debug'], $config['truncate_debug']);

        try {
            $this->ig->login($config['login'], $config['password']);
        } catch (\Exception $e) {
            echo 'Something went wrong: '.$e->getMessage()."\n";
            exit(0);
        }
    }

    /**
    * Upload image and caption text to instagram account
    *
    * @var $ad
    */
    public function uploadTextAndImage($ad)
    {
        $photoFilename = public_path('uploads/deals/') . $ad->images[0]->title;
        $captionText = $this->captionText($ad);

        try {
            $photo = new \InstagramAPI\Media\Photo\InstagramPhoto($photoFilename);
            $this->ig->timeline->uploadPhoto($photo->getFile(), ['caption' => $captionText]);
        } catch (\Exception $e) {
            echo 'Something went wrong: ' . $e->getMessage() . "\n";
        }
    }

    /**
     * Upload album and caption text to instagram account
     *
     * @var $ad
     */
    public function uploadTextAndAlbum($ad)
    {
        $captionText = $this->captionText($ad);
        $images = [];

        $mediaOptions = [
            'targetFeed' => \InstagramAPI\Constants::FEED_TIMELINE_ALBUM,
            // Uncomment to expand media instead of cropping it.
            //'operation' => \InstagramAPI\Media\InstagramMedia::EXPAND,
        ];

        foreach ($ad->images as $image) {
            $images[] = [
                'type' => 'photo',
                'file' => public_path('uploads/deals/') . $image->title
            ];
        }

        foreach ($images as &$image) {
            $validMedia = new \InstagramAPI\Media\Photo\InstagramPhoto($image['file'], $mediaOptions);

            if ($validMedia === null) {
                continue;
            }

            try {
                $image['file'] = $validMedia->getFile();
                // We must prevent the InstagramMedia object from destructing too early,
                // because the media class auto-deletes the processed file during their
                // destructor's cleanup (so we wouldn't be able to upload those files).
                $image['__media'] = $validMedia; // Save object in an unused array key.
            } catch (\Exception $e) {
                continue;
            }

            if (!isset($mediaOptions['forceAspectRatio'])) {
                // Use the first media file's aspect ratio for all subsequent files.
                /** @var \InstagramAPI\Media\MediaDetails $mediaDetails */
                $mediaDetails = new \InstagramAPI\Media\Photo\PhotoDetails($image['file']);
                $mediaOptions['forceAspectRatio'] = $mediaDetails->getAspectRatio();
            }
        }
        unset($image);

        try {
            $this->ig->timeline->uploadAlbum($images, ['caption' => $captionText]);
        } catch (\Exception $e) {
            echo 'Something went wrong: ' . $e->getMessage() . "\n";
        }
    }

    /*
     * Text for instagram post
     *
     * @var $ad
     */
    public function captionText($ad)
    {
       return $text = $ad->description;
    }
}