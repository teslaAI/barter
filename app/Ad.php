<?php

namespace App;

use Cookie;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Bigperson\VkGeo\Models\City;
use Auth;

class Ad extends Model
{
    protected $appends = ['tagList', 'usersFavorite', 'thumbnailUrl'];
    protected $fillable = ['title', 'description', 'city_id', 'category_id', 'phone', 'user_id', 'tags', 'active'];
    const TOTAL_ADS = 30;

    public function tags()
    {
    	return $this->belongsToMany(Tag::class);
    }

    public function category()
    {
    	return $this->belongsTo(Category::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function images()
    {
    	return $this->hasMany(Image::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function chats()
    {
        return $this->belongsToMany(Chat::class);
    }

    public function incrementViews()
    {
        $this->timestamps = false;
        $this->increment('views');
        $this->timestamps = true;
    }

    public function getThumbnailUrlAttribute()
    {
        if ($this->images()->count())
        {
            $thumbnail = $this->images()->first();

            return 'uploads/card/' . $thumbnail->title;
        }

        return 'uploads/placeholder.jpg';
    }

    public function getTagListAttribute()
    {
        return $this->tags->pluck('id')->toArray();
    }

    public function getUsersFavoriteAttribute()
    {
        if ($user = Auth::user())
        {
            $count = Favorite::where('ad_id', $this->id)->where('user_id', $user->id)->count();

            if ($count > 0)
            {
                return true;
            }
        }

        return false;
    }

    public function getPageTitleAttribute()
    {
        $tags = implode(', ', $this->tags->pluck('name')->toArray());
        return "Меняю $this->title на $tags";
    }

    public static function location(Request $request)
    {
        if ($request->location) {
            Cookie::forget('location');
            $cookie = Cookie::queue('location', $request->location);

            return $geo_location = $request->location;
        } elseif (Cookie::get('location')) {
            return $geo_location = Cookie::get('location');
        } else {
//            return $geo_location = geoip()->getLocation($ip = null)->state_name;
            return 'Россия';
        }
    }
}
