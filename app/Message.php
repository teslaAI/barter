<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $appends = ['attachedImages'];
    protected $fillable = ['message', 'chat_id', 'user_id', 'is_viewed'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function makeMessageViewed()
    {
        $this->timestamps = false;
        $this->is_viewed = true;
        $this->save();
        $this->timestamps = true;
    }

    public function attachedImages()
    {
        return $this->hasMany(AttachedImage::class);
    }

    public function getAttachedImagesAttribute()
    {
        return $this->attachedImages()->get();
    }
}
