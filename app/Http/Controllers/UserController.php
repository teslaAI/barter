<?php

namespace App\Http\Controllers;

use Auth;
use App\Avatar;
use Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use File;

class UserController extends Controller
{
    public function settings()
    {
        return view('settings');
    }

    public function storeSettings(Request $request)
    {
        $user = Auth::user();
        $phone = null;

        if ($request['phone']) {
            $phone = substr($request['phone'], 2);
        }

        $user->name = $request['name'];
        $user->email = $request['email'];
        $user->phone =  $phone;
        $user->city = $request['city'];

        if ($request['password']) {
            $user->password = bcrypt($request['password']);
        }

        $user->receiver = $request['receiver'] ? $request['receiver'] : 0;

        $user->save();

        $this->uploadAvatar();

        return redirect()->back()->with('message', 'Настройки сохранены');
    }

    public function uploadAvatar()
    {
        if (Input::file('file')) {
            $avatar = Avatar::where('user_id', Auth::user()->id)->first();

            if ($avatar) {
                File::delete('uploads/avatars/' . $avatar->name);
                $avatar->delete();
            }

            Auth::user()->avatar_url = null;
            Auth::user()->save();

            $input = Input::file('file');

            $extension = $input->getClientOriginalExtension();
            $fileName = rand(11111, 99999) . '.' . $extension;
            $destinationPath = public_path('uploads/avatars/' . $fileName);

            Image::make($input)->fit(140, 140)->save($destinationPath);
            Avatar::create(['name' => $fileName, 'user_id' => Auth::user()->id]);
        }
    }
}
