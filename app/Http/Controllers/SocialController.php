<?php

namespace App\Http\Controllers;

use Auth;
use Redirect;
use Socialite;
use App\User;

class SocialController extends Controller
{
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->user();

        $authUser = $this->findOrCreateUser($user);

        Auth::login($authUser, true);

        if (!$authUser->email) {
            return Redirect::to('/settings')->with('warning', 'Заполните поле email, чтобы завершить регистрацию');
        }

        return Redirect::to('/');
    }

    private function findOrCreateUser($user)
    {
        if ($user->email && $authUser = User::where('email', $user->email)->first()) {
            return $authUser;
        } elseif ($authUser = User::where('social_id', $user->id)->first()) {
            return $authUser;
        }

        return User::create([
            'name' => $user->name,
            'email' => $user->email,
            'social_id' => $user->id,
            'password' => bcrypt(str_random(10)),
            'avatar_url' => $user->avatar,
            'confirmed' => 1,
        ]);
    }
}
