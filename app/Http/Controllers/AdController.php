<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Ad;
use App\Category;
use App\Tag;
use App\User;
use App\Favorite;
use Image;
use Illuminate\Support\Facades\Input;
use LocalizedCarbon;
use Response;
use File;
use Bigperson\VkGeo\Models\Region;
use Bigperson\VkGeo\Models\City;
use ATehnix\VkClient\Client;
use Cookie;
use App\Jobs\ProcessAd;

class AdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $parameter
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $parameter = null)
    {
//        $geo_location = Ad::location($request);
//
//        $region = Region::where('title', $geo_location)->first();
//
//        if (!$region) {
//            $region = Region::whereId(1053480)->first();
//        }

        $cities = City::where('region_id', 1030371)->get()->pluck('id');

        $builder = Ad::orderBy('created_at', 'desc')->where('active', 1);

//        if ($geo_location != 'Россия') {
//            $builder->whereIn('city_id', $cities);
//        }

        $tags = Tag::topTags(1030371, $cities);
        $categories = Category::getAll($main = true);
        $search = $request['search'];
        $cat = null;
        $category_name = null;
        $tagName = null;
        $param_link = null;
        $search_param = null;

        if ($request['category']) {
            $parameter = $request['category'];
            $segment = 'category';
        } elseif ($request['tag']) {
            $parameter = $request['tag'];
            $segment = 'tag';
        } else {
            $segment = \Request::segment(1);
        }

        if ($parameter) {
            switch ($segment) {
                case 'category':
                    $cat = $parameter;
                    $param_link = '/category/' . $parameter;
                    $category = Category::whereSlug($parameter)->firstOrFail();
                    $category_name = $category->name;
                    if ($category->parent_id) {
                        $builder->where('category_id', $category->id);
                    } else {
                        $ids = $category->children()->select('id')->get()->pluck('id');
                        $builder->whereIn('category_id', $ids);
                    }
                    break;
                case 'tag':
                    $tagName = $parameter;
                    $param_link = '/tag/' . $parameter;
                    $builder->whereHas('tags', function ($query) use ($parameter) {
                        $query->where('name', $parameter);
                    });
                    break;
            }
        }

        if ($search && $request->get('option') == 0) {
            $builder->whereHas('tags', function ($query) use ($search) {
                $query->where('name', 'LIKE', "%$search%");
            });
            $search_param = 'tags';
        } elseif ($request->get('option') == 1) {
            $builder->where('title', 'LIKE', "%$search%")->orWhere('description', 'LIKE', "%$search%");
            $search_param = 'deals';
        }

        $all_ads = $builder->get()->count();

        $ads = $builder->with('tags')->with('images')->offset($request->get('offset'))->limit(Ad::TOTAL_ADS)->get();
        $offset = $ads->count();
        $option = $request->get('option');

        if ($request->ajax()) {
            return [$ads, $all_ads];
        }

        return view('main', compact('ads', 'tags', 'categories', 'regions', 'offset', 'option', 'search', 'cat', 'category_name', 'tagName', 'param_link', 'search_param', 'all_ads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if (Auth::check()) {
            $categories = Category::getAll();
            $tags = Tag::withCount('ads')->orderBy('ads_count', 'desc')->get();

//            $geo_location = Ad::location($request);

//            $region = Region::where('title', $geo_location)->first();
            $cities = City::where('region_id', 1030371)->get();

            return view('create', compact('categories', 'tags', 'region', 'cities'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::check()) {

            $images = $request->get('images');

            if (auth()->user()) {
                $userId = auth()->user()->id;
            }

            $fields = array_merge($request->only([
                'title',
                'description',
                'category_id',]),
                ['user_id' => $userId, 'phone' => substr($request->input('phone'), 2)],
                ['city_id' => $request['city_id']]);

            $ad = Ad::create($fields);

            if ($images) {
                foreach ($images as $image) {
                    $fields = ['ad_id' => $ad->id, 'title' => $image];
                    \App\Image::create($fields);
                }
            }

            $tags = Tag::checkForNewTags($request);

            $ad->tags()->attach($tags);

//            ProcessAd::dispatch($ad);

            return redirect("show/$ad->id");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ad = Ad::whereId($id)->with('images')->with('category')->with('tags')->firstOrFail();

        $user = User::where('id', $ad->user_id)->firstOrFail();

        $userAds = Ad::where('user_id', $user->id)->with('images')->where('active', 1)->orderBy('created_at')->take(4)->get();

        $ad->incrementViews();

        return view('single', compact('ad', 'user', 'userAds'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if (Auth::check()) {

            $ad = Ad::whereId($id)->firstOrFail();
            if (Auth::user()->id == $ad->user_id) {
                $categories = Category::getAll();
                $tags = Tag::withCount('ads')->orderBy('ads_count', 'desc')->get();
                $regions = Region::all();

                if ($request->ajax()) {
                    return $images = $ad->images;
                }

//                $geo_location = Ad::location($request);

//                $region = Region::where('title', $geo_location)->first();
                $cities = City::where('region_id', 1030371)->get();

                $city = City::where('id', $ad->city_id)->first()->title;

                return view('create', compact('ad', 'categories', 'tags', 'regions', 'region', 'cities', 'city'));
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::check()) {

            $ad = Ad::whereId($id)->firstOrFail();
            if (Auth::user()->id == $ad->user_id) {

                $images = $request->get('images');

                if (auth()->user()) {
                    $userId = auth()->user()->id;
                }

                $city = $request['city_id'] ? ['city_id' => City::where('id', $request['city_id'])->first()->id] : [];

                $fields = array_merge($request->only([
                    'title',
                    'description',
                    'category_id']),
                    ['user_id' => $userId, 'phone' => substr($request->input('phone'), 2)],
                    $city);

                $ad->update($fields);

                if ($images) {
                    foreach ($images as $image) {
                        $fields = ['ad_id' => $ad->id, 'title' => $image];
                        \App\Image::create($fields);
                    }
                }

                Tag::syncTags($ad, $request);

                return redirect("show/$ad->id");
            }
        }
    }

    /**
     * Deactivate the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function toggleActiveAd($id)
    {
        $ad = Ad::whereId($id)->firstOrFail();
        $active = !$ad->active;

        $ad->active = $active;
        $ad->save();

        return 'ok';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Auth::check()) {
            $ad = Ad::whereId($id)->firstOrFail();
            if (Auth::user()->id == $ad->user_id) {
                $ad->delete();

                return redirect("user_ads");
            }
        }
    }

    public function uploadImages()
    {
        $input = Input::file('file');

        $extension = $input->getClientOriginalExtension();

        $fileName = rand(11111, 99999) . '.' . $extension;

        $destinationPath = public_path('uploads/deals/' . $fileName);
        $destinationForThumb = public_path('uploads/card/' . $fileName);

        $background = Image::canvas(1200, 900, '#3B3B3B');

        $image = Image::make($input);

        $image->backup()->heighten(900)->widen(1200, function($constraint){
            $constraint->upsize();
        });

        $background->insert($image, 'center');
        $background->save($destinationPath);
        $upload = Image::make($input)->fit(271, 180)->save($destinationForThumb);

        return $fileName;
    }

    public function deleteImages($image)
    {
        $img = \App\Image::where('title', $image)->first();

        if ($img) {
            \App\Image::destroy($img->id);
        }

        File::delete(public_path('uploads/deals/' . $image));
        File::delete(public_path('uploads/card/' . $image));
    }

    public function userAds(Request $request)
    {
        if (Auth::check()) {
            $builder = Ad::where('user_id', Auth::user()->id)
                ->orderBy('created_at', 'desc')
                ->with('images')->with('tags');

            $author = Auth::user();
            $status = null;

            if ($request['status'] == 'deactivated') {
                $ads = $builder->where('active', 0);
                $status = $request['status'];
                $all_ads = $builder->get()->count();
            } elseif ($request['status'] == 'favorite') {
                $ads = Favorite::getById(Auth::user()->id);
                $status = $request['status'];
                $all_ads = $ads->get()->count();
            } else {
                $ads = $builder->where('active', 1);
                $all_ads = $builder->get()->count();
            }

            $ads = $ads->offset($request->get('offset'))
                ->limit(Ad::TOTAL_ADS)->get();

            $offset = $ads->count();

            if ($request->ajax()) {
                return [$ads, $offset, $all_ads];
            }

            return view('user', compact('status', 'author'));
        }
    }

    public function authorAds(Request $request, $id)
    {
        $builder = Ad::where('user_id', $id)
            ->orderBy('created_at', 'desc')
            ->with('images')->with('tags')
            ->where('active', 1);

        $all_ads = $builder->get()->count();

        $ads = $builder->limit(Ad::TOTAL_ADS)->offset($request->get('offset'))->get();

        $author = User::whereId($id)->firstOrFail();

        $offset = $ads->count();

        if ($request->ajax()) {
            return [$ads, $offset, $all_ads];
        }

        return view('user', compact('author'));
    }

    public function setRemovefavorite($id)
    {
        if (Auth::check()) {
            $user = Auth::user()->id;

            $favorite = Favorite::where('user_id', $user)->where('ad_id', $id)->first();

            if (!$favorite) {
                Favorite::create([
                    'user_id' => Auth::user()->id,
                    'ad_id' => $id
                ]);
            } else {
                $favorite->delete();
            }
        } else {
            return 1;
        }
    }

    public function search(Request $request)
    {
        if ($request['tag']) {
            $search = $request['tag'];
            $items = Tag::withCount('ads')
                ->where('name', 'LIKE',  "%$search%")
                ->orderBy('ads_count', 'desc')
                ->limit(12)->get();
        } else {
            $search = $request['ad'];
            $items = Ad::where('title', 'LIKE', "%$search%")->where('active', 1)->limit(12)->get();
        }

        return $items;
    }

    public function apiLocation(Request $request)
    {
        $api = new Client;

        $api->setDefaultToken("6c764db26c764db26c764db2806c1718c666c766c764db236fcae1ef08fe18b7a386af7");
        $response = $api->request('database.getCities', ['country_id' => 1, 'q' => $request['location']]);

        return $response;
    }

    public function storeAdFromInstagram(Request $request)
    {
        $images = $request->get('images');

        $fields = [
            'title' => 'Instagram ad',
            'description' => $request['description'],
            'category_id' => 1,
            'user_id' => 1,
            'city_id' => 1,
            'active' => 0
        ];

        $ad = Ad::create($fields);

        if ($images) {
            foreach ($images as $image) {
                $fields = ['ad_id' => $ad->id, 'title' => $image];
                \App\Image::create($fields);
            }
        }

        ProcessAd::dispatch($ad);

        return redirect()->back()->with('message', 'Объявление появится на странице через несколько секунд');
    }
}
