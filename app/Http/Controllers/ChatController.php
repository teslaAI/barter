<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ad;
use Auth;
use App\Chat;
use File;
use LocalizedCarbon;
use App\Message;
use App\Events\MessagePosted;
use App\Events\MessageViewed;
use Illuminate\Support\Facades\Input;
use Image;
use App\User;

class ChatController extends Controller
{
    /**
     * Show chat or create if it doesn't exist.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param $parameter
     * @param $id
     * @param $author_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function chat(Request $request, $parameter = null, $id = null, $author_id = null)
    {
        $user = Auth::user();

        if (null != $id && null != $author_id) {
            $ad = Ad::whereId($id)->first();
            $author = User::whereId($author_id)->firstOrFail();

            if ($user->id == $author_id) {
                return redirect('/');
            }

            $chat = Chat::whereHas('users', function ($query) use ($user) {
                $query->where('users.id', $user->id);
            })->whereHas('users', function ($query) use ($author_id) {
                $query->where('users.id', $author_id);
            })->where('ad_id', $id)->first();

            if (!$chat && $request['message']) {
                $fields = ['ad_id' => $id, 'user_id' => $author_id, 'ad_title' => $ad->title, 'ad_image' => $ad->images()->first() ? $ad->images()->first()->title : ''];
                $chat = Chat::create($fields);
                $chat->users()->sync([$user->id, $author_id]);

                $request['chat_id'] = $chat->id;
                $this->createMessage($request);

            } elseif ($request['message']) {
                $request['chat_id'] = $chat->id;
                $this->createMessage($request);
            }

            $messages = Message::where('chat_id', $chat->id)->with('user')->get();

            $notViewedMess = Message::where('is_viewed', 0)->where('chat_id', $chat->id)->where('user_id', '!=', Auth::user()->id)->get();

            foreach ($notViewedMess as $message) {
                $message->makeMessageViewed();
            }

            if ($request->ajax()) {
                return [$messages, $author, $ad, $chat];
            }
        }

        // Sort and return chats for specified request
        if ($parameter) {
            switch ($parameter) {
                case 'all':
                    $builder = Chat::orderBy('id');
                    break;
                case 'proposes':
                    $builder = Chat::where('user_id', '=', $user->id);
                    break;
                case 'offers':
                    $builder = Chat::where('user_id', '!=', $user->id);
                    break;
            }
        }

        $chats = $builder->whereHas('users', function ($query) use ($user) {
            $query->where('user_id', $user->id);
        })->with('messages')->with('not_viewed_messages')->with('users')->get();


        $chats = $chats->sortByDesc(function ($chat, $key) {
            $message = $chat->messages->first();
            if ($message) {
                return $message->created_at;
            }
        })->values();

        if ($request->ajax()) {
            return $chats;
        }

        return view('chat');
    }

    /**
     * Managing chat messages.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function createMessage(Request $request)
    {
        $user = Auth::user();

        $message = $user->messages()->create([
            'message' => $request->get('message'),
            'chat_id' => $request->get('chat_id'),
        ]);

        if ($request->get('images')) {
            foreach ($request->get('images') as $image) {
                $fields = ['message_id' => $message->id, 'name' => $image];
                \App\AttachedImage::create($fields);
            }
        }

        broadcast(new MessagePosted($message, $user))->toOthers();

        $message = Message::whereId($message->id)->first();

        return $message;
    }

    /**
     * Destroy chat.
     *
     * @param $id
     * @return mixed
     */
    public function destroyChat($id)
    {
        $chat = Chat::whereHas('users', function ($query) {
            $query->where('users.id', Auth::user()->id);
        })->whereId($id)->firstOrFail();

        if ($chat) {
            $chat->delete();
        }
    }

    /**
     * Uploading attached images.
     *
     * @return mixed
     */
    public function uploadFiles()
    {
        $input = Input::file('file');

        $extension = $input->getClientOriginalExtension();

        $fileName = rand(11111, 99999) . '.' . $extension;

        $destinationPath = public_path('uploads/attached/' . $fileName);

        $upload = Image::make($input)->fit(1200, 900)->save($destinationPath);

        return $fileName;
    }

    public function deleteFiles($image)
    {
        File::delete(public_path('uploads/attached/' . $image));
    }
}
