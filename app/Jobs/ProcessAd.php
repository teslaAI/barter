<?php

namespace App\Jobs;

use App\Ad;
use App\Services\InstaService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProcessAd implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $service;
    protected $ad;

    /**
     * Create a new job instance.
     *
     * @var $ad
     *
     * @return void
     */
    public function __construct(Ad $ad)
    {
        $this->ad = $ad;
    }

    /**
     * Execute the job.
     *
     * @var $service
     * @return void
     */
    public function handle(InstaService $service)
    {
        if (count($this->ad->images) > 1) {
            $service->uploadTextAndAlbum($this->ad);
        } else {
            $service->uploadTextAndImage($this->ad);
        }
    }
}
