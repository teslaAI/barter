function ChatHeight() {
    var ChatH = $('.chat-frame-dialog-wrapper').outerHeight();
    var innerChatH = $('.chat-frame-dialog-wrapper ul').outerHeight();
    var marginChat = ChatH - innerChatH;
    if (marginChat > 0) {
        $('.chat-frame-dialog-wrapper ul').css('margin-top', marginChat - 3);
    }
}

function scrollBottomChat() {
    $('.chat-frame-dialog-scroll').scrollTop($('.chat-frame-dialog-scroll ul').outerHeight());
    $('.chat-frame-dialog-scroll').scrollTop($('.chat-frame-dialog-scroll ul').outerHeight());
}

function ChatPageHeight() {
    var WindowHeight = $(window).height();
    if(window.matchMedia('(max-width: 900px)').matches)
    {
        $('.chat-page .chat-content').css('height', WindowHeight);
    }else if(window.matchMedia('(max-width: 1300px)').matches) {
        $('.chat-page .chat-content').css('height', WindowHeight - 128);
    }else{
        $('.chat-page .chat-content').css('height', WindowHeight - 168);
    }
}

$(document).ready(function() {

    scrollBottomChat();

    ChatHeight();

    ChatPageHeight();

    if (!$.cookie('noShowWelcome')) $('.bluetip_wrapper').show();
    $(".close_button").click(function() {
        $(".bluetip_wrapper").hide();
        $.cookie('noShowWelcome', false);
    });

    $('#search-form-select').selectize({
        create: true,
        sortField: 'text'
    });

    $('#input-city').selectize({
        sortField: 'text'
    });

    $('#input-locations').selectize({
        sortField: 'text'
    });

    $('#input-cat').selectize();

    $('#input-options').selectize({
        plugins: ['remove_button'],
        delimiter: ',',
        persist: false,
        maxItems: 6,
        create: function(input) {
            return {
                value: input,
                text: input,
            }
        },
        createFilter: function(input) { return input.length <= 25; }
    });

    // function random(owlSelector){
    //     owlSelector.children().sort(function(){
    //         return Math.round(Math.random()) - 0.5;
    //     }).each(function(){
    //         $(this).appendTo(owlSelector);
    //     });
    // }
    //
    // var $owl = $( '.owl-carousel' );
    // $owl.on('initialize.owl.carousel', function(event){
    //     var selector = $('.owl-carousel');
    //     random(selector);
    // });
    //
    //
    // $(".owl-carousel").owlCarousel({
    //     loop:true,
    //     margin:20,
    //     nav:false,
    //     responsiveClass:true,
    //     responsive:{
    //         0:{
    //             items:1,
    //         },
    //         600:{
    //             items:2,
    //         },
    //         1000:{
    //             items:2,
    //         }
    //     }
    // });

    $(document).on('keyup change', '#input-options-selectized', function () {
        if ($(this).val().length > 25) {
            $(this).closest('.form-item').find('.tip3').addClass('active');
        } else {
            $(this).closest('.form-item').find('.tip3').removeClass('active');
        }
    });

    $('.selectize-input').on('click', function(e){
        if($(this).hasClass('focus')){
            e.stopPropagation();
        }
    })

    $('#input-location').selectize({
        maxItems: 3,
        plugins: ['remove_button'],
        delimiter: ',',
        persist: false,
        create: function(input) {
            return {
                value: input,
                text: input
            }
        }
    });

    $('.auth-link').on('click', function(){
        $('.popup-auth').addClass('active');
        return false
    })

    $('.auth-chat').on('click', function(){
        $('.popup-auth-chat').addClass('active');
        return false
    })

    $('.popup-auth-wrapper').on('click', function(e){
        e.stopPropagation()
    })

    $('.header-profile').on('click', function(){
        $(this).toggleClass('active')
        return false
    })

    $('.profile-popup .popup-close').on('click', function(){
        $('.header-profile').removeClass('active')
        return false
    })

    $('.popup-auth-wrapper .popup-close').on('click', function(){
        $('.popup-auth').removeClass('active')
        return false
    })

    $('.location-popup .popup-close').on('click', function(){
        $('.location').removeClass('active')
        return false
    })

    $('.popup-tail').on('click', function(e){
        e.stopPropagation()
    })

    $('.location-link, .location-link-resp').on('click', function () {
        $('.location').toggleClass('active')
        return false;
    })
    $('.location').on('click', function (e) {
        e.stopPropagation();
    })

    $(document).on('click', '.chats-item', function () {
        $('.chats').removeClass('active');
    });

    $(document).on('click', '.fh-left .chat-resp-menu', function () {
        $('.chats').addClass('active');
    });

    $('.attach-deal').on('click', function () {
        $('.popup-attach-deal').toggleClass('active')
        return false;
    })

    $('.phone-btn').on('click', function () {
        $('.popup-show-phone').toggleClass('active')
        return false;
    })

    $('.chat-btn').on('click', function () {
        $('.popup-show-chat').toggleClass('active')
        return false;
    })

    $('.popup-auth-wrapper').on('click', function (e) {
        e.stopPropagation();
    })

    $('.register_link').on('click', function () {
        $('.popup-auth-wrapper').addClass('signup');
        return false
    })

    $('.signin_link').on('click', function () {
        $('.popup-auth-wrapper').removeClass('signup');
        return false
    })

    $('.mob-menu-wrapper').on('click', function (e) {
      $(this).toggleClass('active');
      $('.resp-menu-wrapper').toggleClass('active');
      e.stopPropagation();
    })

    $('.resp-menu-wrapper').on('click', function (e) {
        e.stopPropagation();
    })

    var inputsTel = document.querySelectorAll('input[type="tel"]');

    Inputmask({
        "mask": "+7 (999) 999-99-99",
        showMaskOnHover: false
    }).mask(inputsTel);

    $(document).on('click', '.search-form-results li', function () {
        var searchtext = $(this).text();
        var searchid = $(this).attr('id');
        $('.search-form-input').val(searchtext);
        $('.search-form-location').val(searchtext);
        $('.search-location-input').val(searchid);
        $('.search-form-submit').click();
        $('.search-form-results').hide();
    });

    $('.chat-message-input-text').keypress(function(e){
        if(e.which == 13 && !e.shiftKey) {
            $(this).closest("form").submit();
            e.preventDefault();
            return false;
        }
    });

    $('.nav .nav-item').each(function(){
        if($(this).hasClass('active')){
            $(this).find('ul').slideDown(200);
        }
    });

    $('.nav .nav-item').on('click', function(){
        $('.submenu').slideUp(200);
        var self = this;
        $('.nav .nav-item').each(function(){
            if (self != this) $(this).removeClass('active');
        });
        if (!$(this).hasClass('active')) {
            $(this).find('ul').slideDown(200);
            $(this).addClass('active');
        } else {
            $(this).find('ul').slideUp(200);
            $(this).removeClass('active');
        }
    })

    $('.favorite').on('click', function(e){
        $(this).toggleClass('active');
    })

    if($('.search-form-opt .checkbox').is(':checked')) {
        $('.search-form-input').attr("placeholder", "Поиск среди предложений");
    }else{
        $('.search-form-input').attr("placeholder", "Поиск среди того, что хотят");
    }

    $('.search-form-opt label').on('click', function () {
        if(!$('.search-form-opt .checkbox').is(':checked')) {
            $('.search-form-input').attr("placeholder", "Поиск среди предложений");
        }else{
            $('.search-form-input').attr("placeholder", "Поиск среди того, что хотят");
        }
    })

    $(document).on('click', function(e) {
        $('.location').removeClass('active');
        $('.popup-auth').removeClass('active');
        $('.header-profile').removeClass('active');
        $('.tip3').removeClass('active');
        $('.mob-menu-wrapper').removeClass('active');
        $('.resp-menu-wrapper').removeClass('active');
    })

    $('.profile-menu li').on('click', function(){
        $('.profile-menu li').removeClass("active");
        $(this).addClass('active');
    })
});

$(document).click(function(event) {
    if(!$(event.target).closest('.search-form-results').length) {
        if($('.search-form-input .search-location-input').is(":focus")) {
            $('.search-form-results .search-location-input').show();
        }else {
            $('.search-form-results .search-location-input').hide();
        }
    }
});

$(window).resize(function() {
    ChatHeight();
    ChatPageHeight();
    scrollBottomChat();
})

jQuery(function($){
    var fileInput = document.getElementById("upload-image");
    var uploaded = document.getElementById("load-image");
    var photo = document.getElementsByClassName("load-photo");

    if (fileInput) {
        fileInput.addEventListener("change",function(e){
            var files = this.files
            showThumbnail(files)
        },false)
    }


    function showThumbnail(files){
        var file = files[0]

        $(photo).show().focus().click().remove();

        var image = document.createElement("img");
        image.setAttribute("class", "load-photo");
        var thumbnail = document.getElementById("thumbnail");
        image.file = file;
        thumbnail.appendChild(image)

        var reader = new FileReader()
        reader.onload = (function(aImg){
            return function(e){
                aImg.src = e.target.result;
                $(uploaded).show().focus().click().remove();
            };
        }(image))
        var ret = reader.readAsDataURL(file);
        var canvas = document.createElement("canvas");
        var ctx = canvas.getContext("2d");
        image.onload= function(){
            ctx.drawImage(image,100,100)
        }
    }
});

var $createAdForm = $('.form-ad-wrapper');

var iconImageSvgUrl = $('#my-awesome-dropzone').data('img-url');

Dropzone.options.myAwesomeDropzone = {
    dictDefaultMessage: '<div class="dz-u-image"><img src="'+ iconImageSvgUrl +'"><p>Добавить фото</p></div>',
    addRemoveLinks: true,
    dictRemoveFile: 'Удалить',
    dictCancelUpload: 'Отмена',
    maxFiles: 9,
    maxFilesize: 10,
    acceptedFiles: 'image/*',
    dictMaxFilesExceeded: 'Можно загрузить только 9 изображений',
    dictInvalidFileType: 'Можно загружать только изображения',
    accept: function(file, done) {done();},
    success: function(data){
        var image = data.xhr.response
        var dataAttrs = ' data-name="' + data.name +'" data-lastmod="' + data.lastModified + '" ';
        $('.form-ad-wrapper').append('<input type="hidden" name="images[]" '+ dataAttrs +' value="'+ image +'">')
    },
    error: function() {
        $('.image-error').css("display", "block");
    },
    removedfile: function(file) {
        var _ref;
        var val;
        $createAdForm.find('input[type=hidden]').each(function(){
            if ($(this).data('name') == file.name && $(this).data('lastmod') == file.lastModified)
            {
                val = $(this).val()
                $(this).remove()
            }
        })
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: $('#my-awesome-dropzone').data('rm-url')+val,
            method: 'post',
            data: val
        })
        console.log('removed')
        return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
    },
    headers: {
        'X-CSRF-Token': $('input[name="_token"]').val()
    }
}