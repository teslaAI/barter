var gulp = require('gulp'),
	sass = require('gulp-sass'),
	browserSync = require('browser-sync');

gulp.task('default', function(){
    //
});

gulp.task('sass', function () {
    return gulp.src('public/scss/*.scss')
        .pipe(sass({
            // outputStyle: 'compressed'
        })
        .on('error', sass.logError))
        .pipe(gulp.dest('public/css'))
        .pipe(browserSync.reload({stream: true}))
});

gulp.task('browser-sync', function() {
    browserSync({
        proxy: 'http://barter/',
        host: 'barter',
        open: 'external',
        notify: false
    });
});

gulp.task('watch', ['browser-sync', 'sass'], function () {
    gulp.watch('public/scss/*.scss', ['sass']);
    gulp.watch('resources/views/*.blade.php', browserSync.reload);
    gulp.watch('public/js/*.js', browserSync.reload);
});